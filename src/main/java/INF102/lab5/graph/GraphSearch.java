package INF102.lab5.graph;


import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.HashSet;

/**
 * This class is used to conduct search algorithms of a graph
 */
public class GraphSearch<V> implements IGraphSearch<V> {

    private IGraph<V> graph;

    public GraphSearch(IGraph<V> graph) {
        this.graph = graph;
    }

    	// u is the starting node, v is the ending node
    public boolean connected(V u, V v) {
        // have to check if the nodes actually exist in the graph
        if (!graph.hasNode(u) || !graph.hasNode(v)) {
            return false;
        }

        // do a Breadth-First Search to check if a path exists from u to v
        
        Queue<V> queue = new LinkedList<>();
        Set<V> visited = new HashSet<>();

        // start at u
        queue.add(u);
        visited.add(u);

        while (!queue.isEmpty()) {
            V current = queue.poll();

            // check if the current node (the one reached) is v, meaning there is a path to v
            if (current.equals(v)) {
                return true;
            }

            // check other nearby nodes
            for (V neighbor : graph.getNeighbourhood(current)) {
                if (!visited.contains(neighbor)) {
                    queue.add(neighbor);
                    visited.add(neighbor);
                }
            }
        }

        // if no path is found, false is returned
        return false;
    }

}
